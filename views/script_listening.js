var stepTime = 0
var stepTimeIncrease = 0
var numberStep = 5

const player = new Plyr('#player', {
    controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume','settings', 'pip', 'airplay', 'fullscreen'],
    settings: ['quality','speed'],
    speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player.on('ready', event => {
    var videoDuration = player.duration
    stepTime = videoDuration/numberStep
    stepTimeIncrease = videoDuration/numberStep
})
player.on('timeupdate', event => {
    const instance = event.detail.plyr;
    console.log(player.currentTime)
    console.log(player.duration)
    if (player.currentTime >= stepTime) {
        player.restart()
        stepTime += stepTimeIncrease
    }
  });


// captions
// https://www.youtube.com/api/timedtext?v=XJGiS83eQLk&type=list
// https://www.youtube.com/api/timedtext?v=XJGiS83eQLk&lang=en&fmt=vtt


// Hover effect for forward and backward buttons
const buttonForward = document.getElementById('forward')
buttonForward.addEventListener('mouseenter', function() {
    buttonForward.src = '../assets/forward-hover.png'
})
buttonForward.addEventListener('mouseout', function() {
    buttonForward.src = '../assets/forward.png'
})
const buttonBackward = document.getElementById('backward')
buttonBackward.addEventListener('mouseenter', function() {
    buttonBackward.src = '../assets/backward-hover.png'
})
buttonBackward.addEventListener('mouseout', function() {
    buttonBackward.src = '../assets/backward.png'
})

function setCaption(currentCaptionPart) {
    var box = document.getElementById('caption')
    box.innerHTML = ""
    var captionNode = document.createTextNode(currentCaptionPart)
    box.appendChild(captionNode)
}