# PIE

Software com interface gráfica para auxiliar no aprendizado de idiomas. O nome foi inspirado na língua PIE (Proto-Indo-European) que hipoteticamente seria a ancestral comum de todas as línguas hoje existentes. Utilizou-se o framework Eletron para o desenvolvimento do app.

## Recursos


- Busca por videos do youtube em qualquer idioma
- Assistir os vídeos com repetição com extensão progressiva
    
    Consiste em repetir partes do vídeo desde o início cada vez maiores até que se reptoduza o vídeo completo

- ...
